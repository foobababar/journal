# ubuntu server as NAS on qemu

## links
[article talking about this specific issue](https://apiraino.github.io/qemu-bridge-networking/)    
[article on redhat about setting up set bridge](https://www.redhat.com/sysadmin/setup-network-bridge-VM)


## qemu
install qemu     
install ubuntu server on qemu   

create disk    
`qemu-img create -f qcow2 ubuntu.img 20G`

boot from iso    
`qemu-system-x86_64 -enable-kvm -cdrom ubuntu.iso -boot menu=on -drive file=ubuntu.img -m 4G -cpu host -vga virtio -display sdl,gl=on`

boot from hard drive     
`qemu-system-x86_64 -enable-kvm -boot menu=on -drive file=ubuntu.img -m 4G -cpu host -vga virtio -display sdl,gl=on`

## SSH into server
Cant ping guest from host.     
Need to read on network bridges.    
probably need to add a qemu command line option    
vm is on an isolated network, can't ping host    

## setup NFS (Network File System)

## setup SAMBA
[tutorial for ubuntu](https://ubuntu.com/tutorials/install-and-configure-samba#1-overview)

## second attempt
[qemu documentation on networking](https://en.wikibooks.org/wiki/QEMU/Networking)
need tap interface

network bridge helper

need tap device

[multiple solutions to this problem](https://www.linux-kvm.org/page/Networking)
[youtube video tutorial](https://www.youtube.com/watch?v=DYpaX4BnNlghttps://www.youtube.com/watch?v=DYpaX4BnNlg)
^ this worked

use virt-manager if gui, else use virsh     
virsh is the CLI,  equivalent to virt-manager (GUI)

all virt-manager does is call qemu

qemu guest host file :  /var/log/libvirt/qemu/ubuntu22.04.log

[gemu to virsh ??](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/virtualization_administration_guide/sub-sect-domain_commands-converting_qemu_arguments_to_domain_xml)


