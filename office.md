# LIBREOFFICE JOURNAL

## WRITER

### templates

### hyperlinks

### headings ?
* Styles -> Manage Styles


### page break
* insert page break : control + return

### headers and footers
* top and bottom of pages. Not titles
* avoid for now ?

### conventions ?
* font 12 for normal txt
* font 13 for headers


## CALC


### make graph from data 
* select datas then click on "insert chart"

### merge row
* select row -> right click -> merge cells

### always show first row
* select first row -> View -> Freeze rows and columns

### move row up/down
* select entire row -> hold alt key -> click on any cell on select row -> drag & drop

### insert row
* right click on cell -> insert -> entire row/column

### sort data

### filter data
* click on "Autofilter"

### product
* `=PRODUCT(A1,B1)`
* `=PRODUCT(A1,B1)`

### sum
* `=SUM(A1:A5)`
* `=SUM(A1,A2)`

### get formula result
* right click on cell -> copy -> right click other cell -> paste special -> unformatted text

### copy/paste column

### concat
*  `=CONCAT(A2:A9)`

### data file formats
* json, yaml, sql, xml (see examples)


### dollar sign to lock in place
* `=$B2*I$2`


### don't evaluate formula in cell 
* right click on cell -> format cells -> Category -> Text

### rows->columns ; columns->rows
* copy table -> paste special -> transpose


### pivot table 
* TODO


### how many unique values in range
* `=SUMPRODUCT(1/COUNTIF(A2:A430,A2:A430))`


### conditional formating, color cell if in range ...
* [link](https://ask.libreoffice.org/t/formula-to-change-background-colour-if-y-or-n-is-in-cell/54015/3)


### select all data
* ctrl + shift + end


### calculate age libreoffice calc
* `=DATEDIF(C15,TODAY(),"y")`


### split column into 2
* select column -> data -> text to column










