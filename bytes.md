# EVERYTHING ABOUT BITS, BYTES, MEMORY @, GDB, ...

## ASCII stuff
see big ascii table

some useful ascii values :

```
|ASCII| DEC |   
=============
|  A  |  65 |   
-------------
|  Z  |  90 |   
-------------
|  a  |  97 |   
-------------
|  z  | 122 |   
-------------
|space|  32 |   
-------------
|  0  |  48 |   
-------------
|  9  |  57 |   
-------------
```
see complete ascii table


## hex
[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]  <- DEC       
[0,1,2,3,4,5,6,7,8,9,0A,0B,0C,0D,0E,0F]  <- HEX


## bytes
* 8 bits = 1 byte
* 1 byte -> [0, 255]
* 11111111 (b2) -> 255 (b10) -> FF (b16)
* **1 byte has 256 possible values !**

in hex editor, 2 numbers make 1 byte.    
for example : 

```
... 00 0A FE FF ...
    -- -- -- --
```
*00 is 1 byte*    
*0A is 1 byte*     
*FE is 1 byte*     
*FF is 1 byte*

**u16 = 2 bytes = 256^2 = 65536 possible values = [0..65535]**    
**u32 = 4 bytes = 256^4 = 4294967296 possible values**    


## bitwise operators
### truth table

```
A | B | and | or | xor | nor | -> |
===================================
0 | 0 |   0 |  0 |   0 |   1 |  1 |
0 | 1 |   0 |  1 |   1 |   0 |  1 |
1 | 0 |   0 |  1 |   1 |   0 |  0 |
1 | 1 |   1 |  1 |   0 |   0 |  1 |
```

### & (and)
0b01 & 0b10 == 0b00

### | (or)
0b01 | 0b10 == 0b11 == 3 

### ^ (xor)
a^b == 1 if (a==1 and b==0) or (a==0 and b==1)

### bitshift right
0b01 >> 1 == 0b00

### bitshift left
0b01 << 1 == 0b10

### ~ not
~0b00000000 == 0b11111111



## binary to decimal
ex : convert 0b10010111 to decimal     
(0b prefix means binary)

```
0b10010111 = (1 * 2^0) + (1 * 2^1) + (1 * 2^2) + (0* 2^3) + (1 * 2^4) + (0 * 2^5) + (0 * 2^6) + (1 * 2^7)
           = 1 + 2 + 4 + 0 + 16 + 0 + 0 + 128
           = 151
```

## hex to decimal
ex : convert 0xAE to decimal     
(0x prefix means hexadecimal)

```
0xAE = (E * 16^0) + (A * 16^1)
     = 14 + (10 * 16)
     = 174
```

## two's complement
TODO 

## floats representation
### mantisse stuff 
TODO


## assembly starter pack ?
TODO

## gdb starter pack
### run with gdb
* gcc main.c -o main
* `gdb ./main`
* file must be named main.c (or main.zig) 
* for names different than main, gdb won't be able to find symbols.

### some useful gdb commands
* `list` : show code
* `break <lineNb>` : add breakpoint on lineNb
* `break <functionName>`
* `run` : run the binary. Will stop @ breakpoint.
* `next` : execute next line, but stay in scope
* `step` : exectute next line. If there is a function, go inside.
* `print <varname>` : print value of varname
* `print &<varname>` : print @ of varname in memory ?
* `p/u <varname>` : print value of varname as unsigned int
* `x <address>` : show byte at address ?

### in practice
TODO : gdb simple examples

### references
* https://stackoverflow.com/questions/3305164/how-to-modify-memory-contents-using-gdb
* https://en.wikipedia.org/wiki/ASCII
