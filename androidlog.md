# ANDROID LOGS

## setup
* [official website](https://developer.android.com/studio)
* android-studio-2022.3.1.18-linux.tar.gz
* install 32 bits dependencies `sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386`
* `tar -xf android-studio-2022.3.1.18-linux.tar.gz`
* `bash android-studio/studio.sh`
* follow wizard







## hello world

### AndroidManifest.xml
```
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools">

    <application
        android:allowBackup="true"
        android:dataExtractionRules="@xml/data_extraction_rules"
        android:fullBackupContent="@xml/backup_rules"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/Theme.MyApplication"
        tools:targetApi="31">
        <activity
            android:name=".MainActivity"
            android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
    </application>

</manifest>
```

### MainActivity.java
```
package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
```
### activity_main.xml
```
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Hello World!"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

## on R class

## useful data
* user installed apps located at : `/data/app/com.example.myapp`



## camera panasonic app
* from MainActivity.java
* "connect to camera ; connect to first url ; connect to second url ; if doesn't work connect to third url ; connect to fourth url to shoot ; connect to fifth to cancel shooting
```
URL url = new URL("http://192.168.54.1/cam.cgi?mode=accctrl&type=req_acc&value=0&value2=Lumix%20Link%20Desktop");
URL url = new URL("http://192.168.54.1/cam.cgi");
URL url = new URL("http://192.168.54.1/cam.cgi?mode=accctrl&type=req_acc&value=0&value2=Vexia%20Fcs");
URL url = new URL("http://192.168.54.1/cam.cgi?mode=camcmd&value=capture");
URL url = new URL("http://192.168.54.1/cam.cgi?mode=camcmd&value=capture_cancel");
```


## TextView

## use bluetooth

## simple music player

## adb
 access smartphone : `/run/user/1000/gvfs/mtp:host=MediaTek_4049D_55BU8PZS7DLNVK8L`

* `sudo apt install adb`
* `adb devices`
```
* daemon not running; starting now at tcp:5037
* daemon started successfully
List of devices attached
55BU8PZS7DLNVK8L	unauthorized
```

* `adb shell su`
* `ls`
```
acct
cache
charger
config
custom
d
data
default.prop
dev
enableswap.sh
etc
factory_init.project.rc
factory_init.rc
file_contexts
fstab.mt6570
init
init.aee.rc
init.common_svc.rc
init.environ.rc
init.modem.rc
init.mt6570.rc
init.mt6570.usb.rc
init.nvdata.rc
init.project.rc
init.rc
init.trace.rc
init.usb.rc
init.xlog.rc
init.zygote32.rc
meta_init.modem.rc
meta_init.project.rc
meta_init.rc
mnt
nvcfg
nvdata
oem
persist
proc
property_contexts
protect_f
protect_s
root
sbin
sdcard
seapp_contexts
selinux_version
sepolicy
service_contexts
storage
sys
system
ueventd.mt6570.rc
ueventd.rc
vendor
```

* `adb pull data/app/com.example.pana2-1/base.apk`




## apk decompiler
* [jadx](https://github.com/skylot/jadx) 
* apktool


## ndk
