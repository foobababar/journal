# doom emacs log 

## sources : 
* https://github.com/doomemacs/
* https://www.youtube.com/watch?v=37H7bD-G7nE

## config files
* ~/.config/doom/init.el
* ~/.config/doom/config.el
* ~/.config/doom/packages.el


## misc
* M <=> Alt
* SPC <=> SPACE
* (package! tldr) : inside packages.el ; install package
* everything is buffer 
* scratch buffer


## shortcuts 
* swap between panes : Ctrl w w

* M-x find-file : open file in new buffer
* M-x ibuffer : see all buffers
* M-x term

* SPC f r : find recent files 
* SPC b k : kill buffer
* SPC h r r : reload doom ; apply changes
* SPC b p : goto previous buffer
* SPC b n : goto next buffer
* SPC w v : split vert
* SPC w c : close split 
* SPC w w : switch split 