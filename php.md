# PHP learning journal

## links
* [php handbook](https://www.php.net/manual/en/)
* [w3school](https://www.w3schools.com/php/default.asp)



## prep
* `php -v`
* `php myfile.php`


## hello world 
```
<?php
    echo "hello world !\n";
?>
```





## types
```
<?php
    $fl = 0.72;
    $bool = true;
    $int = 2;

    $str = "foo"; 
    $alsostr = 'ba';
    $strtoo = 'c';

    echo $fl.$bool.$int.$str.$alsostr.$strtoo."\n";

    echo gettype($strtoo) == gettype($alsostr);
    echo gettype($str) == gettype($alsostr);
?>
```






## declare const
```
<?php
    define('bar', "bar");
    print(bar);
?>
```








## strings
```
<?php
    $str = "foobar";
    echo strlen($str);
    // echo count($str); //does not work
    
    echo substr($str, 3); //bar
?>
```





## arrays 
```
<?php
    // arrays are like python dictionaries
    $jedi = array("obiwan", "luke", "anakin");

    $sith = array(0 => "sidious", 1 => "maul", 2 => "vader");

    echo $jedi[0].",".$jedi[1].",".$jedi[2]."\n";

    echo count($jedi);

    $force_users = array_merge($jedi, $sith);
    // many more useful array_functions

    print_r($force_users);
?>
```


## list 
TODO ?


## for loop 

```
<?php
    for ($i = 0; $i<=10; $i++) {
        echo $i."\n";
    }
    
    // --------

    $starters = array("squirtle", "bulbazaur", "charmander");

    foreach($starters as $starter){
        echo $starter."\n";
    }
?>
```



## while loop

```
<?php
    $i = 0;
    while ($i<=255) {
        echo $i."\n";
        $i++;
    }
?>
```





## functions
TODO

## useful functions

## types conversions

## bizarre stuff
* isset
* is_null
* in_array

## echo vs print ?
* echo has no return value, is faster

## php oop ?
