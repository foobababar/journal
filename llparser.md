# LL parsing algorithm

## NAMES
* LL = left to right, leftmost deviation
* topdown parser


## GRAMMAR
```
L -> I;L | eof
I -> id = E
E -> T+E | T-E | T
T -> F * T | F / T | F
F -> id | nb | (E)
```