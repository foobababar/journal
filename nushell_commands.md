# NUSHELL COMMANDS


## list all files under 4Ko
```
ls | where size == 4.0KiB
```


## remove all directories in specific dir
```
ls | where type != file | each {|dir| rm -r $dir.name }
```


## read csv file
```
cat LASTNAMES.csv | from csv --separator ";"
```


## create range of files
```
1..5 | each {||} | into string | each {|name| 'file' + $name } | each {|fname| save $fname}
```

## get single row
```
ls | select 10000
```

## left join and filter
```
cat FIRSTNAMES.csv | from csv --separator ";" | join -l (cat LASTNAMES.csv | from csv --separator ";") IDS IDS | filter {|x| $x.FIRSTNAMES == PIERRE }
```