# sdl
compile sdl2 from source 

make statically linked library

## data
* version : 2.0.20
* /usr/include/SDL/SDL.h
* /usr/include/SDL2/SDL.h

* sdl binaries
```
/usr/lib64/libSDL_mixer.so
/usr/lib64/libSDL2_gfx-1.0.so.0.0.2
/usr/lib64/libSDL2_image-2.0.so.0
/usr/lib64/libSDLmain.a
/usr/lib64/libSDL2_ttf-2.0.so.0.18.0
/usr/lib64/libSDL_mixer-1.2.so.0
/usr/lib64/libSDL2_gfx-1.0.so.0
/usr/lib64/libSDL2_image-2.0.so.0.2.3
/usr/lib64/libSDL.so
/usr/lib64/libSDL2_mixer.so
/usr/lib64/libSDL_net.so
/usr/lib64/libSDL2-2.0.so.0.18.2
/usr/lib64/libSDL_ttf-2.0.so.0.10.1
/usr/lib64/libSDL-1.2.so.0.11.4
/usr/lib64/libSDL_mixer-1.2.so.0.12.0
/usr/lib64/libSDL2-2.0.so.0
/usr/lib64/libSDL2_net.so
/usr/lib64/libSDL2_mixer-2.0.so.0
/usr/lib64/libSDL_sound-1.0.so.1.0.2
/usr/lib64/libSDL2_ttf.so
/usr/lib64/libSDL2_ttf-2.0.so.0
/usr/lib64/libSDL_net-1.2.so.0
/usr/lib64/libSDL_image-1.2.so.0
/usr/lib64/libSDL2.so
/usr/lib64/libSDL_ttf.so
/usr/lib64/libSDL2_gfx.so
/usr/lib64/libSDL2_mixer-2.0.so.0.2.2
/usr/lib64/libSDL-1.2.so.0
/usr/lib64/libSDL_net-1.2.so.0.8.0
/usr/lib64/libSDL2_net-2.0.so.0
/usr/lib64/libSDL_image-1.2.so.0.8.4
/usr/lib64/libSDL2_net-2.0.so.0.0.1
/usr/lib64/libSDL_ttf-2.0.so.0
/usr/lib64/libSDL2_image.so
/usr/lib64/libSDL2_test.a
/usr/lib64/libSDL_sound.so
/usr/lib64/libSDL2main.a
/usr/lib64/libSDL_image.so
/usr/lib64/libSDL_sound-1.0.so.1
```

## more data
* file ending with .a = static libraries
* file ending with .o = object file

## more data
* -L /../lib64
* -I /../include

## on build.zig
* exe.linkSystemLibrary("c");
* exe.addIncludePath(...); (-I)
* exe.addLibraryPath(...); (-L)


## cross compile to get statically linked
* zig build -Dtarget=x86_64-linux-musl      (c import failed)
* zig build -Dtarget=native_linux_musl      (c import failed)
* zig build -Dtarget=native-native-musl     (works, but cant exec)

## optimize ?
* zig build -Doptimize=ReleaseSafe
* zig build -Doptimize=ReleaseSmall
* zig build -Doptimize=ReleaseFast

## ldd command result on zig-out/bin/pixdraw
```
        linux-vdso.so.1 (0x00007ffd21cf7000)
        libSDL2-2.0.so.0 => /usr/lib64/libSDL2-2.0.so.0 (0x00007f01307bb000)
        libX11.so.6 => /usr/lib64/libX11.so.6 (0x00007f013067a000)
        libXi.so.6 => /usr/lib64/libXi.so.6 (0x00007f0130667000)
        libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f0130613000)
        libc.so.6 => /lib64/libc.so.6 (0x00007f0130434000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f013095d000)
        libm.so.6 => /lib64/libm.so.6 (0x00007f01302ec000)
        libdl.so.2 => /lib64/libdl.so.2 (0x00007f01302e5000)
        librt.so.1 => /lib64/librt.so.1 (0x00007f01302db000)
        libxcb.so.1 => /usr/lib64/libxcb.so.1 (0x00007f01302b2000)
        libXext.so.6 => /usr/lib64/libXext.so.6 (0x00007f013029e000)
        libXau.so.6 => /usr/lib64/libXau.so.6 (0x00007f0130299000)
        libXdmcp.so.6 => /usr/lib64/libXdmcp.so.6 (0x00007f013028f000)
```

## SDL_Init function location
~/Downloads/SDL2-2.26.5/src/SDL.c


## debug symbols
system shared library doesn't have debug symbols
can't debug for example SDL_UpdateTexture()

TODO : compile a simple sdl hello world with local version of sdl2  
TODO : compile or find a binary with debug info


## hello world 
```
// sdl hello world
// use to test sdl setup
// build with `gcc sdlhw.c -lSDL2 -o out`  
#include "SDL2/SDL.h"
#include <stdio.h>


int main(int argc, char* argv[]) {
    SDL_Window *window;  

    SDL_Init(SDL_INIT_VIDEO);

    window = SDL_CreateWindow("SDL hello world !", 0, 0, 640, 420, 0);

    if (window == NULL) {
        printf("Could not create window: %s\n", SDL_GetError());
        return 1;
    }

    int quit = 0;

    while (!quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event))  {
            switch (event.type) {
                case SDL_QUIT:  
                    quit = 1;
                    break;
                default :
                    break;
            }
        }
    }


    SDL_DestroyWindow(window);

    SDL_Quit();
    return 0;
}
```

static : libSDL2.a
dynamic : libSDL2.so



## ubuntu packages
* package libsdl2-dev contains all sdl2 header file, ie : 
```
SDL.h
SDL_events.h
...
```

* package libsdl2-2.0-0 contains libSDL2.a








## successful build
`gcc main.c -I include/ build/.libs/libSDL2.so -lm -o out`

* when building sdl from source, .a and .so files appear in build/.libs
* .la file are garbage


## sxiv stuff
* sxiv requires libimlib2 libimlib2-dev libexif-dev

TODO : find flip code on sxiv

"img_flip" in image.c, line 692


TODO : all sxiv into 1 file ?



* makefile output : 
```
CC autoreload_inotify.o
CC commands.o
CC image.o
CC main.o
CC options.o
CC thumbs.o
CC util.o
CC window.o
LINK sxiv
```

* these 2 lines will build sxiv :

`gcc -c allfiles.c -c -I/usr/include/freetype2 -D_XOPEN_SOURCE=700 -DHAVE_GIFLIB=1 -DHAVE_LIBEXIF=1 -lc -lexif -lgif -lImlib2 -lX11 -lXft -lfontconfig -std=c99`

`gcc -o sxiv allfiles.o -I/usr/include/freetype2 -D_XOPEN_SOURCE=700 -DHAVE_GIFLIB=1 -DHAVE_LIBEXIF=1 -lc -lexif -lgif -lImlib2 -lX11 -lXft -lfontconfig -std=c99`

* gcc doesnt accept wildcards



* objdump also works on .o files


* build sxiv with debug infos : `make CFLAGS=-g`
* `objdump -t libSDL2.so` : will show all functions inside lib

* zig sxiv build system
`alias zig=/home/foobar/apps/zig-linux-x86_64-0.11.0-dev.3892+0a6cd257b/zig`


`zig cc -c autoreload_inotify.c -I/usr/include/freetype2`
`zig cc -c commands.c -I/usr/include/freetype2`

main.c becomes c_main.c


build.zig
```
const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
ls
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "sxiv_zig",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    exe.linkSystemLibrary("c");
    exe.addCSourceFile("c/autoreload_inotify.c", &[_][]const u8{"-I/usr/include/freetype2"});
    exe.addCSourceFile("c/commands.c", &[_][]const u8{"-I/usr/include/freetype2"});
    exe.addCSourceFile("c/image.c", &[_][]const u8{"-I/usr/include/freetype2"});
    exe.addCSourceFile("c/main.c", &[_][]const u8{"-I/usr/include/freetype2"});

    exe.addCSourceFile("c/options.c", &[_][]const u8{"-I/usr/include/freetype2"});
    exe.addCSourceFile("c/thumbs.c", &[_][]const u8{"-I/usr/include/freetype2"});
    exe.addCSourceFile("c/util.c", &[_][]const u8{"-I/usr/include/freetype2"});
    exe.addCSourceFile("c/window.c", &[_][]const u8{"-I/usr/include/freetype2"});


    exe.linkSystemLibrary("Imlib2");
    exe.linkSystemLibrary("X11");
    exe.linkSystemLibrary("Xft");
    exe.linkSystemLibrary("exif");
    exe.linkSystemLibrary("gif");
    exe.linkSystemLibrary("fontconfig");




    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

 
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    const run_unit_tests = b.addRunArtifact(unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}

```



main.zig 
```
extern fn c_main() c_int;
pub fn main() u8 {
    return @intCast(c_main());
}
```

not working : "cannot allocate memory"








### 1/7/23
* files 
```
autoreload_inotify.c
autoreload_nop.c (not used)
commands.c
image.c
main.c 
options.c
thumbs.c
util.c
window.c
```


* inotify
* inotify - monitoring filesystem events
* [manpages](https://man7.org/linux/man-pages/man7/inotify.7.html)











### 2/7/23

build.sh
```
#!/bin/sh
freetype="-I/usr/include/freetype2"
flags="-Wall"


if [ ${#} -eq 1 ]; then 
    if [[ "$1" == "clean" ]]; then 
        echo "cleaning..."
        [ -f *.o ] && rm *.o    
        [ -f sxiv ] && rm sxiv
        exit 0
    elif [[ "$1" != "clean" ]]; then
        echo "unrecognized option..."
        exit 1
    fi
fi



[ -f "autoreload_nop.c" ] && rm autoreload_nop.c

source_files=$(ls *.c)

echo "building object files ..."
for i in $source_files
do
        echo $i
        gcc -c "$i" "$flags" "$freetype"
done


echo "linking..."

obj_files=$(ls *.o)
gcc -o sxiv $obj_files "$freetype" -lc -lexif -lgif -lImlib2 -lX11 -lXft -lfontconfig -std=c99
```

build.sh v3

```
#!/bin/sh
freetype="-I/usr/include/freetype2"
flags="-Wall"


function cleanup() {
    # cleanup workspace
    rm *.o  2>/dev/null
    rm sxiv 2>/dev/null
}

if [ ${#} -eq 1 ]; then
        if [[ "$1" == "clean" ]]; then
                echo "cleaning..."
                cleanup
                exit 0
        elif [[ "$1" == "link" ]]; then
                link=1
        elif [[ "$1" == "run" ]]; then 
                link=1
                run=1
        else 
                echo "unrecognized option: $1"
                exit 1
        fi
fi

CC="zig cc"
CC2="zig build-obj"

echo "building objects..."
$CC -c autoreload_inotify.c $freetype
$CC -c commands.c -c $freetype 
$CC2 image_2.zig $freetype -lc
$CC -c image.c -c $freetype 
$CC -c main.c -c $freetype 
$CC -c options.c -c $freetype 
$CC -c thumbs.c -c $freetype 
$CC -c util.c -c $freetype 
$CC -c window.c -c $freetype 


[[ "$link" -ne 1 ]] && exit 0

echo "linking..."
obj_files="autoreload_inotify.o commands.o image.o main.o options.o thumbs.o util.o window.o"
zig_obj_files="image_2.o"

$CC -o sxiv $obj_files $zig_obj_files "$freetype" -lc -lexif -lgif -lImlib2 -lX11 -lXft -lfontconfig -std=c99


[[ "$run" -ne 1 ]] && exit 0

echo "running test..."
./sxiv icon/128x128.png
```


build.sh v4

```
#!/bin/sh

set -o nounset

freetype="-I/usr/include/freetype2"
flags="-Wall"
options="-DHAVE_GIFLIB=1 -DHAVE_LIBEXIF=1"

CC="zig cc"
CC2="zig build-obj"

function cleanup() {
    # cleanup workspace
    echo "cleaning..."
    rm *.o  2>/dev/null
    rm sxiv 2>/dev/null
}


function build() {
    echo "building objects..."
    $CC -c autoreload_inotify.c $freetype $options
    $CC -c commands.c -c $freetype $options
    $CC2 image_2.zig $freetype -lc
    $CC -c image.c -c $freetype $options
    $CC -c main.c -c $freetype $options
    $CC -c options.c -c $freetype $options
    $CC -c thumbs.c -c $freetype $options
    $CC -c util.c -c $freetype $options
    $CC -c window.c -c $freetype $options
}


function link() {
    echo "link object files"
    local obj_files="autoreload_inotify.o commands.o image.o main.o options.o thumbs.o util.o window.o"
    local zig_obj_files="image_2.o"
    $CC -o sxiv $obj_files $zig_obj_files "$freetype" -lc -lexif -lgif -lImlib2 -lX11 -lXft -lfontconfig -std=c99
}


function run() {
    echo "running test..."
    ./sxiv icon/128x128.png
}

if [ ${#} -eq 0 ]; then
    echo "bar" 
    cleanup 
    build 
    link  
elif [ ${#} -eq 1 ]; then
    if [[ "$1" == "clean" ]]; then
            cleanup
    elif [[ "$1" == "run" ]]; then
            cleanup && build && link && run
    else 
            echo unrecognized argument : $1
            exit 1
    fi
else 
        echo "wrong args"
        exit 1
fi
```

### 7/4/23

in c :
```
void foo(int *z) {
	*z = 12; 
}


float bar;
foo(&bar);
```





in zig : 
```
fn foo(z: *i32) void {
    z.* = 12;
}

var bar: i32 = undefined;
foo(&bar);
```




beware of global variables !



### 5/7/23

array of functions !
```
    const FnType = fn () callconv(.C) void;
    const imlib_flip_op = [3]FnType { sxiv.imlib_image_flip_horizontal, sxiv.imlib_image_flip_vertical, sxiv.imlib_image_flip_diagonal};
    imlib_flip_op[1]();
```

TODO : rewrite all macros in classic c code (in c files)
















## imagemagick stuff
TODO : find flip code on imagemagick    
`convert -flop input.png output.png`    
`convert -flip input.png output.png`    





* build imagemagick :
./configure
make -j3




```
bash-5.1$ find . -name "*.so"
./MagickCore/.libs/libMagickCore-7.Q16HDRI.so
./MagickWand/.libs/libMagickWand-7.Q16HDRI.so
./Magick++/lib/.libs/libMagick++-7.Q16HDRI.so
bash-5.1$ find . -name "*.a"
./MagickCore/.libs/libMagickCore-7.Q16HDRI.a
./MagickWand/.libs/libMagickWand-7.Q16HDRI.a
./Magick++/lib/.libs/libMagick++-7.Q16HDRI.a
```


### 9/7/23
on local imagemagick build : 
* `utilities/magick convert -flop images/rose.png rose2.png`
* `utilities/.libs/magick convert -flop images/rose.png rose3.png`  <- this is the binary file !


* Magick++ : C++ api. Irrelevant ?
* FlopImage : transform.c : line 1322
* `gcc -c transform.c -I..`
* build only MagickCore ?


### 12/7/23

* `./configure --disable-installed --without-magick-plus-plus --without-perl`











## gcc stuff 
* do only preprocessing : `gcc -E file.c > out.c`
* generate object files `gcc -c file.c`
* link object files : `gcc -o bin *.o`


## on HCL/HSV color
[wikipedia](https://en.wikipedia.org/wiki/HSL_and_HSV)    
[stackoverflow conversion algo](https://stackoverflow.com/questions/3018313/algorithm-to-convert-rgb-to-hsv-and-hsv-to-rgb-in-range-0-255-for-both)    
[rgb2hsv php stackoverfldow](https://stackoverflow.com/questions/1773698/rgb-to-hsv-in-php?noredirect=1&lq=1)    
[github gits both functions](https://gist.github.com/zyphlar/55dea0fae7914ff8eb4a)


## gdb stuff
* debug exe : `gdb --args sxiv icon/128x128.png`
* break filename:linenumber : `break image.c:437`
