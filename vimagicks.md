# vim magick tricks

## replace all occurrences of "foo" in current line 
* `s/0/2/g`

## search for "foo" inside current code block
* `/\%Vfoo`

## replace all occurences of "foo" with "bar"
* `%s/foo/bar/g`

## highlight all search results
* `:set hlsearch`
* `/myword`

## delete until character "_"
* `dt_`
