# NETWORKING JOURNAL



## router 
[cloudflare routers def](https://www.cloudflare.com/learning/network-layer/what-is-a-router/)    
forwards data packets between networks          
uses routing tables     
send packets to their destinations efficiently     
mostly layer 3




## switch
[cloudflare def of switch](https://www.cloudflare.com/learning/network-layer/what-is-a-network-switch/)    
connects devices on network      
packets switching    
can operate at layer 2 (data) (uses mac @)     
or at layer 3 (network) (uses ip @)    
only forwards data to intended destination, cannot send data to multiple devices 
  
  
    
    
## hub
"poor man’s switch"    
strictly worse than switch    
layer 1    
"dumb device"    
connect multiple devices together. Make them act like a single network segment    
sends data to multiple devices, without restrictions.    
receives signal on 1 port and send it to all other ports



## MAC @ (Media Access Control @)
assigned by device manufacturer    
6 groups of 2 hex numbers    
2 devices on 2 different networks can share same mac@    
layer 2        
hardware @, stored in firmware of network card


## port
16 bits number    
identify specific application/service



## socket
endpoint for data delivery    
ip@ + port number    
example : 192.168.1.12:24




## IPV4
4 numbers in range [0..255] ( 4 bytes ) separated by '.'    
ex : 19.168.1.24    
=> 2^32 possible ip@ (a little less, some are reserved)

classes of IP@ : ...

private IP@ are like 192.168.xxx.xxx    
for example : 192.168.1.29

loopback : ping oneself. Use to troubleshoot    
example : 127.0.0.1

there are 2 parts in ipv4 : network part and host part.    
subnet mask identifies these parts    

example :  TODO





## subnetting
[study-ccna on subnetting](https://study-ccna.com/subnetting-explained/)    
[study-ccna on subnet creation](https://study-ccna.com/create-subnets/)      
[networkChuck practice subnetting](https://www.youtube.com/watch?v=B1vqKQIPxr0)    
[subnet recap from freecodebootcamp](https://www.freecodecamp.org/news/subnet-cheat-sheet-24-subnet-mask-30-26-27-29-and-other-ip-address-cidr-network-references/)    
[practical networking on youtube](https://www.youtube.com/watch?v=BWZ-MHIhqjM)


`192.168.1.12/24`    
"/24" is called `CIDR notation`
the "/24" is the subnet mask = 255.255.255.0 = 11111111.11111111.11111111.00000000    
( 24 times "1" )      
TODO


2 addresses are reserved :
* network @ (also called network id) : 192.168.1.0 (first ip@ on subnetwork)
* broadcast @ : 192.168.1.255 (last ip@ in subnetwork) communicate with all @ on subnet


netmask : 255.255.255.0    
TODO : netwask/subnetmask definition & usage    
* host bits : all zeros in netmask
* network bits : all ones in netmask

TODO : subnetting increment definition

**254 usable ip addresses (where ?)** because : 254 = 256 - network@ - broadcast@    
TODO : subnet mask formula with `&`

subnetting table

```
128   64   32   16    8    4    2    1     
128  192  224  240  248  252  254  255
/25  /26  /27  /28  /29  /30  /31  /32
```
use ^ to calculate 7 values of network v

1. network id
2. broadcast id
3. first host ip
4. last host ip
5. next network @
6. number of ip@
7. CIDR/subnet
[practical networking video on youtube explaining how to use table](https://youtu.be/5-wlfAdcmFQ)


### VLSM
[study-ccna on vlsm](https://study-ccna.com/variable-length-subnet-mask-vlsm/)
TODO





## classes of ip@
TODO ????



## IPV6  
eight groups of 4 hex digits    
ex : 2001:0db8:0000:0000:0000:8a2e:0370:7334    
16^32 == (2^4)^32 == 2^128 possible addresses (approx) 

`formula : base^numberOfDigits`




## NAT (Network Address Translation)
IPV4    
nevices on same local network have private IP@    
router gets 1 public IP@.    
all devices on same network use this public IP@    


## nc

### nc server
`nc -l -p 8080`    
`nc -l -p 8080 -o file`  :  print all activities into file


### nc client
`nc host port` : create TCP connection to the given port on the given target host    
`nc localhost 8080` 


## tcpdump
use tcpdump to capture packets    
like wireshark    


## ifconfig
equivalent to `ip addr`
`man ifconfig`    
are ifconfig results safe to publish online ???


## virtualization software
QEMU    
TODO


## traceroute
traceroute - print the route packets trace to network host


### traceroute demo
`traceroute google.com`

```
traceroute to google.com (216.58.215.46), 30 hops max, 60 byte packets
 1  lan.home (192.168.1.1)  0.793 ms  1.197 ms  1.650 ms
 2  80.10.239.153 (80.10.239.153)  16.994 ms  17.309 ms  17.551 ms
 3  92.184.149.18 (92.184.149.18)  17.794 ms  18.132 ms  18.323 ms
 4  92.184.149.14 (92.184.149.14)  18.575 ms  18.791 ms  19.058 ms
 5  ae56-0.ncidf206.rbci.orange.net (81.253.131.118)  222.396 ms  222.168 ms  230.005 ms
 6  ae45-0.niidf202.rbci.orange.net (193.252.98.157)  230.170 ms  215.126 ms  215.956 ms
 7  193.252.137.74 (193.252.137.74)  218.409 ms  218.697 ms  218.964 ms
 8  72.14.202.232 (72.14.202.232)  220.418 ms google-47.gw.opentransit.net (193.251.255.116)  221.041 ms 72.14.202.232 (72.14.202.232)  220.780 ms
 9  * * *
10  142.250.234.40 (142.250.234.40)  224.069 ms 142.251.64.126 (142.251.64.126)  216.905 ms 108.170.235.15 (108.170.235.15)  220.750 ms
11  108.170.244.177 (108.170.244.177)  215.894 ms 108.170.244.240 (108.170.244.240)  215.053 ms 108.170.244.176 (108.170.244.176)  216.398 ms
12  209.85.251.179 (209.85.251.179)  228.557 ms 209.85.251.59 (209.85.251.59)  218.487 ms par21s17-in-f14.1e100.net (216.58.215.46)  215.322 ms
```





## DHCP (Dynamic Host Configuration Protocol)
TODO

## DHCPCD
DHCP client


## wireshark

### some filters
* dns 
* http
* tcp
* ip.addr == 192.168.1.12


### tricks
right click on packet -> follow -> TCP stream    
right click on packet -> conversation filter



## OSI model
abstraction to break networking into several simpler problems    
PDU : Protocol Data Unit    

1. Physical (PDU : bit)
2. Data link / link (PDU : frame)
3. Network (PDU : packet)
4. Transport (PDU : segment, datagram)
5. Session (PDU : data)
6. Presentation (PDU : data)
7. Application (PDU : data)


### protocols in layers

```
application  --  http, dns, telnet, ftp
transport    --  tcp, udp
internet     --  ipv4, ipv6, arp, icmp
link         --  ethernet
```



## HTTP (Hypertext Transfer Protocol)

application layer protocole (layer 7)\
*runs over TCP* \
curl, wget, web browser use http \
RFC9112 \
HTTP1.1 \
HTTP header structure ? \
not encrypted


curl http://www.google.com :

```
GET / HTTP/1.1
Host: www.google.com
User-Agent: curl/8.0.1
Accept: */*

HTTP/1.1 200 OK
Date: Mon, 24 Apr 2023 11:23:06 GMT
Expires: -1
Cache-Control: private, max-age=0
Content-Type: text/html; charset=ISO-8859-1
Content-Security-Policy-Report-Only: object-src 'none';base-uri 'self';script-src 'nonce-pO3kYW2W0zl5JpDxjLPu6Q' 'strict-dynamic' 'report-sample' 'unsafe-eval' 'unsafe-inline' https: http:;report-uri https://csp.withgoogle.com/csp/gws/other-hp
Server: gws
X-XSS-Protection: 0
X-Frame-Options: SAMEORIGIN
Set-Cookie: AEC=AUEFqZc8g49EJHLDAIHHoDfRf3tUedkeSRwqbbb3Oy3AzJRQnBnSCG6alg; expires=Sat, 21-Oct-2023 11:23:06 GMT; path=/; domain=.google.com; Secure; HttpOnly; SameSite=lax
Accept-Ranges: none
Vary: Accept-Encoding
Transfer-Encoding: chunked

38dd
<!doctype html>...</html>
0
```





## HTTPS
can't directly capture https packets with wireshark. They are encrypted.    
TLS means HTTPS    
to decrypt HTTPS traffic, need decryption key    
how to decrypt https traffic ???    
https is http with encryption & verification.    
"HTTPS uses TLS (SSL) to encrypt normal HTTP requests and responses, and to digitally sign those requests and responses." 



## TCP (Transmission Control Protocol) / IP (Internet Protocol)


### IP
[ip header structure](https://study-ccna.com/ip-header/)

### unicast, multicast, broadcast @
[see](https://study-ccna.com/unicast-multicast-and-broadcast-addresses/)    
[and](https://study-ccna.com/types-of-ip-addresses/)    



### encapsulation
add headers and trailers around some data


### TCP vs IP vs HTTP

```
<bottom>

IP
----
TCP
----
HTTP

<top>
```


### what is a protocol ?
Set of rules, etiquette of how "programs should behave"

### TCP layers vs OSI layers
```

  OSI                   TCP
Application          Application
Presentation         Application
Session              Application
Transport            Transport
Network              Network
Link                 Link/Network access
Physical             Link/Network access
```

### TCP
transport level protocol (level 4)    
raw binary data    
reliable but expensive    
reliable, in order delivery    
lossless    
"http is sent using tcp ?"    
http protocol is a member of tcp/ip family    
*tcp runs over ip*    
congestion and flow control    
see TCP segment header diagram    



### TCP three ways handshake
used by TCP    
SYN = synchronization    
ACK = acknowledgement

```
CLIENT ----> SERVER
  SYN   ->    
        <-   SYN-ACK  
  ACK   ->   

```
3 ways handshake then send http request

### TCP terminate connection

```
FIN ->
ACK <-
FIN <-
ACK ->
```



## UDP (User Datagram Protocol)
best effort service    
faster but less reliable than TCP    
unordered delivery
connectionless protocol    
no hanshake    
UDP message minimum size is 64bits (8 bytes)    
very simple


## ARP (Adress Resolution Protocol)
protocole mapping IP@ to MAC@ in LAN


## SSL (Secure Sockets Layer)
security protocol. Developped for HTTP.    
Deprecated. See TLS


## TLS (Transport Layer Security)
used on top of SSL    
SSL replacement    

### TLS handshake
(equivalent to SSL handshake)


## DNS 
[study-ccna on dns](https://study-ccna.com/domain-name-system-dns/)    
associates name to IP address    
application level protocol      
4 types of DNS : TODO    
[verisign animation on dns steps](https://www.verisign.com/en_US/website-presence/online/how-dns-works/index.xhtml)    


### DNS resolver
recursive resolver


### root server

### TLD name server

### domain’s name server



## ICMP (Internet Control Message Protocol)
protocol. Used to diagnose network communication issues. 



## OCSP (Online Certificate Status Protocol)
TODO

## domain name
TODO

## VPN
TODO

## VPS
TODO

## TOR
TODO


## LAN (Local Area Network)
group of connected devices close to each others


## WANS (Wide Area Network)
TODO

## NAS (Network Attached Storage)
TODO : build one 


## proxy
* [tutorial kali, proxy and TOR](https://www.youtube.com/watch?v=LEbAxsYRMcQ)
TODO


## in practice
* see std.net.StreamServer
* send packet with curl and/or wget then capture with wireshark/tcpdump
* [see netkit](https://www.netkit.org/)

## misc
* port 110 is for POP
* port 22 is SSH
* port 21 is FTP
* ethernet cable have 8 cables
* only 4 cables needeed for 100Mbits/s
* ethernet cables are twisted to avoid interfering with each other
* RJ45
* network speed (in bits/s) is equal to lowest speed on network


## quick maths
* 1 octet == 8 bits
* 1 Kbit == 1 000 bits
* 1 Mbit == 1 000 000 bits




## Q&A


## study-ccna chapter lists (for structure)
[study-ccna](https://study-ccna.com/)

1. intro
2. intro to networking
3. CISCO OS IOS
4. transport layer
5. network layer
6. IPV4 adressing
7. subnetting
8. data-link layer
9. physical layer
10. CISCO network devices
11. Life of a packet
12. router and switch basic config 
13. CISCO device management
14. basic network troubleshooting
15. ipv4 routing
16. dynamic routing protocol
17. igp
18. ospf
19. vlan
20. inter-vlan routing
21. dhcp
22. hsrp
23. stp
24. etherchannel
25. switch security
26. acl
27. nat
28. ipv6 routing and adressing
29. wan
30. security concepts
31. network device security
32. network device management
33. qos
34. cloud computing
35. wireless network
36. network automation and programmability


## sources
* [cool site to practice ccna](https://study-ccna.com/what-is-a-network/)
* https://www.cloudflare.com/learning/ddos/glossary/tcp-ip/
* https://www.cloudflare.com/learning/ddos/glossary/user-datagram-protocol-udp/
* https://en.wikipedia.org/wiki/Transmission_Control_Protocol
* https://en.wikipedia.org/wiki/User_Datagram_Protocol
* https://en.wikipedia.org/wiki/OSI_model
* https://en.wikipedia.org/wiki/HTTP
* https://www.reddit.com/r/ccna/
* https://www.youtube.com/watch?v=d-zn-wv4Di8
* https://www.ibm.com/docs/en/cics-ts/5.3?topic=web-internet-tcpip-http-concepts
* https://en.wikipedia.org/wiki/Transport_Layer_Security
* https://datatracker.ietf.org/doc/html/rfc9293
* https://www.cloudflare.com/learning/ssl/why-is-http-not-secure/
* https://www.cloudflare.com/learning/ssl/what-happens-in-a-tls-handshake/
* https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol
* https://www.cloudflare.com/learning/ddos/glossary/internet-control-message-protocol-icmp/
* https://www.youtube.com/watch?v=7CYpjf19GkA
* https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol
* https://study-ccna.com/osi-tcp-ip-models/
* https://en.wikipedia.org/wiki/Subnetwork
* https://www.youtube.com/playlist?list=PLIhvC56v63IKrRHh3gvZZBAGvsvOhwrRF
* [Kurose network book as ppt slides](http://gaia.cs.umass.edu/kurose_ross/ppt.php)
* [fortinet article on ARP](https://www.fortinet.com/resources/cyberglossary/what-is-arp)
* [arp on study-ccna](https://study-ccna.com/arp/)


