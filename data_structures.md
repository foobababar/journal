# common data structures


&nbsp;
&nbsp;
&nbsp;
&nbsp;



## big O notation
means theoretical time it would take to execute program.
* O(1)
* O(n)
* O(n^2)
* O(n log n)
* O(log n)
* O(2^n)
* **we ignore constants**




&nbsp;
&nbsp;
&nbsp;
&nbsp;



## Array
collection (or set) of elements of same memory size.    
Elements are identified by their index (key)    
size can be static of dynamic
### operations
* access -- O(1)
* search -- O(n)
* insert -- O(n)
* delete -- O(n)

easy access to single elements    
harder to insert/delete elements




&nbsp;
&nbsp;
&nbsp;
&nbsp;








## LinkedList
collection of nodes
### singly linked
contains pointer to next node only
### doubly linked
contains pointer to previous node \
contains pointer to next node
### operations
* browse O(n)
* insert O(1) or O(n)
* delete O(1) or O(n)
* access O(n)

easy element insert/delete
harder to access single element




&nbsp;
&nbsp;
&nbsp;
&nbsp;






## stack
last in, first out
### operations
* push O(1)
* pop O(1)




&nbsp;
&nbsp;
&nbsp;
&nbsp;





## Queue
first in, first out
### operations
* insert O(1)
* delete O(1)
* find O(n)



&nbsp;
&nbsp;
&nbsp;
&nbsp;




## HashTable = Hashmap
maps keys to values. \
'python' => 'boring' \
'C' => 'painful' \
'C++' => 'argg' 
### operations

```      best    worst
insert   O(1)    O(n)
delete   O(1)    O(n)
search   O(1)    O(n)
```


&nbsp;
&nbsp;
&nbsp;
&nbsp;




## Binary tree 
tree datastructure. Each node has at most two children.      
* `complete binary tree` : 
* `full binary tree` :


&nbsp;
&nbsp;
&nbsp;
&nbsp;





## Binary Search Tree (BST)
Node based.   
Also called `ordered binary tree` or `sorted binary tree`



```
     (A)
    /   \
   (B)  (C)

```
keys on each node must be greater than any keys on its left    
keys on each node must be smaller than any keys on its right 
### operations
```
           best     | worst
* search   O(log n) | O(n)
* insert   O(log n) | O(n)
* delete   O(log n) | O(n)
```

&nbsp;
&nbsp;
&nbsp;
&nbsp;


## Heap 
complete binary tree + heap property :

### max heap
node (A) is bigger than all of its children  
recursively true

### min heap
node (A) is smaller than all of its children    
recursively true






&nbsp;
&nbsp;
&nbsp;
&nbsp;




## References
* https://en.wikipedia.org/wiki/Data_structure
* https://towardsdatascience.com/seven-7-essential-data-structures-for-a-coding-interview-and-associated-common-questions-72ceb644290
* https://en.wikipedia.org/wiki/Array_(data_structure)
* https://en.wikipedia.org/wiki/Stack_(abstract_data_type)
* https://en.wikipedia.org/wiki/Linked_list
* https://en.wikipedia.org/wiki/Search_tree
* https://en.wikipedia.org/wiki/Hash_table
* https://brilliant.org/wiki/linked-lists/
* https://www.codingninjas.com/codestudio/library/time-and-space-complexity-of-linear-data-structures
* https://en.wikipedia.org/wiki/Queue_(abstract_data_type)
* https://en.wikipedia.org/wiki/Heap_(data_structure)
* https://en.wikipedia.org/wiki/Binary_search_tree
* https://en.wikipedia.org/wiki/Binary_tree