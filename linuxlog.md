# linuxlog














## GENERAL LINUX STUFF

### tty
* tty : teletype termninal
* same as terminal

### system wide azerty keyboard :
* TODO : make better    
* `cp /usr/share/X11/xorg.conf.d/90-keyboard-layout-evdev.conf /etc/X11/xorg.conf.d/`
* change to fr
* `localectl set-keymap fr` (doesn't work)

### no password for user foobar : 
* `visudo` (as root)
* add "foobar ALL=NOPASSWD: ALL" at the end of file 

### fill drive with zeros : 
* (`cat /dev/zero > /dev/sda1`)
* `dd if=/dev/zero of=/dev/sda bs=16M status=progress`
* `dd if=/dev/zero of=/dev/sda1 bs=16M status=progress`

* WARNING : these commands will overwrite partition table too !

### mount encrypted drive
* `cryptsetup luksOpen /dev/sda foo`
* `mount /dev/mapper/foo /mnt/bar`
* startup scripts in rc.local

### tar/untar
* untar tar.gz archive : `tar -xf leafpad-0.8.18.1.tar.gz`    
* compress dir to tar.gz : `tar -czvf leafpad-0.8.18.1.tar.gz leafpad-0.8.18.1/`    

### git (1)
* `git clone https://gitlab.com/foobababar/zigsnake.git`
* `cd zigsnake/`
* `git commit -a`
* `git push -u https://gitlab.com/foobababar/zigsnake.git`

### git (2)
* `mkdir project`
* `git init`
* `touch .gitignore README.md`
* `git add -A`
* `git commit -m 'Initial commit'`
* `git push --set-upstream https://gitlab.com/foobababar/project.git`

### rsync entire system 
* `rsync -rltzuv --progress /home/foobar/Pictures/ /mnt/usb/backup1/`
* `rsync -a --exclude '/tmp' --exclude '/dev' --exclude '/home/backup' --exclude '/proc' --exclude '/dev' / /home/backup`

### rsync directory/ rsync folder
* rsync -a --progress /path/to/src/folder /path/to/dest/folder
* `rsync -a --progress /run/media/foobar/Intenso/ppc/ /run/media/foobar/ddd/ppc/`

### rsync + ssh
* `rsync -a --progress /path/to/src/dir/* foobar@192.168.1.15:~/path/to/dest/folder`

### volume ctrl
* `amixer set Master 30%`    
* `amixer set Master 5%-`    

### cpufreq
* `cpufreq-set -r -g powersave`    
* `mount -t vfat /dev/mmcblk0p1 /mnt`    

### bash shortcuts
* Ctrl+b -> move back 1 char
* Ctrl+f -> move forward 1 char
* Ctlr+d -> delete current char
* Backspace -> delete previous char
* Ctrl+l -> clear
* Ctrl+w -> delete last word
* Ctrl+a -> goto start of line
* Ctrl+e -> goto end of line
* Ctrl+k -> clear line after cursor

### clamav shenanigans
* # `freshclam`
* # `clamscan -r /`

### tool for monitoring devices read/writes
* iostat

### custom keyboard layout
* file is at : `/usr/share/X11/xkb/symbols/`    
* modify this file, then add a <layout> section in file `/usr/share/X11/xkb/rules/evdev.xml`


### set keyboard layout to bepo
* `setxkbmap -layout fr -variant bepo`
* also works on wayland


### mounted partitions on startup

* mount partitions at startup : /etc/fstab    
```
/dev/sda2        swap             swap        defaults         0   0
/dev/sda3        /                ext4        defaults         1   1
/dev/sda1        /boot/efi        vfat        defaults         1   0
#/dev/cdrom      /mnt/cdrom       auto        noauto,owner,ro,comment=x-gvfs-show 0   0
/dev/fd0         /mnt/floppy      auto        noauto,owner     0   0
devpts           /dev/pts         devpts      gid=5,mode=620   0   0
proc             /proc            proc        defaults         0   0
tmpfs            /dev/shm         tmpfs       nosuid,nodev,noexec 0   0
```


### mount iso img
* `sudo mount -o loop ubuntu.iso /mnt/iso`


### file format
* fat32 doesnt support files > 4gb
* exfat ~= fat64 ??. exfat ok on linux
* fat32: old, but std

### zip
* `unzip zipfile.zip -d destfolder`

### find
* `find . -name "foo.c"`


### bad sectors
* wikipedia : 

```
A bad sector in computing is a disk sector on a disk storage unit that is unreadable. Upon taking damage, all information stored on that sector is lost. 
When a bad sector is found and marked, the operating system like Windows or Linux will skip it in the future. 
Bad sectors are a threat to information security in the sense of data remanence. "
```
* soft bad sectors : can be repaired
* hard bad sectors : cannot be repaired

* `e2fsck -cfpv /dev/sda1` : for extX file systems    
* `fsck /dev/sda1`


### make disk img
* `dd if=/dev/sdb of=/mnt/backup.img bs=16M status=progress`

### restore disk img
* `dd if=/mnt/backup.img of=/dev/sda bs=16M status=progress`


### resize partition
* [link](https://www.msp360.com/resources/blog/linux-resize-partition/)

* /dev/sda3 is the partition we want to resize
* `fdisk /dev/sda`
* `p` for print
* note start and end of partition we want to resize
```
Device       Start       End   Sectors   Size Type
/dev/sda1     2048   1050623   1048576   512M EFI System
/dev/sda2  1050624   9439231   8388608     4G Linux swap
/dev/sda3  9439232 234441614 225002383 107.3G Linux filesystem
```
* `d` for delete 
* type nb of partition we want to destroy (here 3)
* `w` for write

* `fdisk /dev/sda`
* `p` for print
```
Device       Start     End Sectors  Size Type
/dev/sda1     2048 1050623 1048576  512M EFI System
/dev/sda2  1050624 9439231 8388608    4G Linux swap
```
* `n` for new
* type partition number (here 3)
* enter first sector (here 9439232)
* enter last sector (here +219G)   
* `w`

* `resize2fs -f /dev/sda3`
* `e2fsck /dev/sda3`
* all done !


### fdisk
* use cfdisk

### create partition
* `mkfs.ext4 /dev/sda3` same as `mkfs -t ext4 /dev/sda3`

### remaining disk space 
* `df -H /dev/sda3`


### compressed backup 
* `dd if=/dev/sda status=progress | gzip -X > /mnt/backup.img.gz`
* `zcat /mnt/backup.img.gz | dd of=/dev/sda status=progress`
* X in [1, 9]
* 1 is fastest
* 9 is highest compression


### show all connected dev (lsblk)
`lsblk`
```
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda      8:0    0 223.6G  0 disk 
├─sda1   8:1    0   512M  0 part /boot/efi
├─sda2   8:2    0     4G  0 part [SWAP]
└─sda3   8:3    0   219G  0 part /
sdb      8:16   1  29.7G  0 disk 
└─sdb1   8:17   1  29.7G  0 part /run/media/foobar/p
sdc      8:32   0 119.2G  0 disk 
└─sdc1   8:33   0 119.2G  0 part /run/media/foobar/Intenso
sr0     11:0    1  1024M  0 rom  
```


### update shared libraries ; copy paste shared library
* copy - paste shared library into /usr/lib64
* run `ldconfig`

### create symlink
* `ln -s existing_file_name symbolic_file_name`

### add new tty font
* put newfont.tty into ~/.local/share/fonts/

### shred entire directory/overwrite entire directory with 0
* `find <dir> -type f -exec shred -uvz {} \;`

### set vim as default text editor
* `export VISUAL=vim`
* `export EDITOR="$VISUAL"`

### list hardware
* `sudo lshw`

### show startup messages
* `sudo dmesg`

### loadkmap/dumpkmap
* for busybox
* dumpkmap > mylayout.kmap
* loadkmap < mylayout.kmap


### disable bluetooth
* `sudo vim /etc/bluetooth/main.conf`
* change line "AutoEnable=true" to "AutoEnable=false"


### add new unlisted resolution xrandr
* with eDP-1 being the display name obtained from `xrandr` command ;
* `xrandr --newmode "1920x1080"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync` 
* `xrandr --addmode eDP-1 1920x1080`
* `xrandr --output eDP-1 --mode 1920x1080`


### xfreerdp
* sometimes, need to add custom resolution xrandr (see above)
* `xfreerdp /v:192.168.1.13 /u:foobar /f`
* ctrl + alt + enter to exit fullscreen



































## LINUX NETWORKING

### dhcp setup
* `ip link`
* `ip link set eth0 up`
* `dhclient`
* `ping gnu.org`

### static ip setup
* `ifconfig eth0 192.168.1.17`
* `ifconfig eth0 netmask 255.255.255.0`
* `ifconfig eth0 broadcast 192.168.1.255`
* `route add default gw 192.168.1.1 eth0`
* `ip route`

### show default gateway 
* `ip route`

### add default gateway 
* `route add default gw 192.168.1.1 eth0`
`mkfs -t ext4 /dev/sda3`

### netcat
* TODO

### router address on home network
* 192.168.1.1
























## BOOTLOADERS


### LILO
* dev halted in 2015

### BIOS 
* basic input/output system

### MBR
* master boot record
* located in first sector of bootable disk
* length 512 bytes


### grub boot process
1. power on
2. BIOS runs POST check
3. MBR
4. GRUB
5. kernel
6. init system

### grub config file
* `vim /etc/default/grub`


### multiboot (1)
* ubuntu installed on hard drive 1
* slackware inslalled on hard drive 2
* grub boots into ubuntu, slackware not added

* `sudo vim /etc/default/grub`
* add "GRUB_DISABLE_OS_PROBER=false"
* `sudo update-grub`
* grub will then find slackware entry on hard drive 2
* at next boot, will have both ubuntu and slackware boot options

### remove efi entry (lilo?)
* go to /boot/efi/EFI and remove unwanted efi entry

### ubuntu disk partition scheme
1. efi -- fat32 -- 512mb -- flags: boot, esp
2. ext4 -- rest of space

### uefi vs bios
uefi <=> efi
* uefi : secure boot
* bios : legacy boot

### grub prompt
* TODO, maybe

### reinstall lilo
* `eliloconfig`

### reinstall grub
```
`sudo mount /dev/sdXY /mnt`
`sudo mount /dev/sdXX /mnt/boot/efi`

`for i in /dev /dev/pts /proc /sys /sys/firmware/efi/efivars /run; do sudo mount -B $i /mnt$i; done`

`sudo chroot /mnt`

`grub-install /dev/sdX`
`update-grub`
`exit`

```
* sdXY = system partition (for example sda1)
* sdXX = efi partition (for ex sda2)
* sdXY = disk (ex: sda)














































## LINUX TOOLBOX


### awk / gawk
* awk is a programming language
* gawk = gnu awk
* text processing
* `awk '{print $0}' awk.txt`
* `awk '{ nlines++ } ; END { print nlines }' awk.txt` 
* TODO : more awk commands


### sed
TODO

### grep
* search for pattern "main" recursively and display line number : `grep -rn "main" *`

### wc
* `cat file.txt | wc`
* `cat file.txt | wc -l`
* `cat file.txt | wc -w`
* `cat file.txt | wc -c`

### find
* find all filenames without extensions : `find . -type f ! -name "*.*"`
































## SLACKWARE


### start wayland kde
* `startkwayland` ?

### user not in sudoers but in wheel
* `visudo`
* uncomment line "%wheel ALL=(ALL) ALL"

### get to tty from greeter 
* ctrl+alt+f6

### slackware partitions list
* EFI (512 mb)
* swap (8G)
* / (rest of hard drive)


### disable bluetooth at startup :    
* `chmod 644 /etc/rc.d/rc.bluetooth`
    
### start|stop|restart bluetooth service (slackware) :    
* `/etc/rc.d/rc.bluetooth start`

### generate startup script after upgrade :    
* `/usr/share/mkinitrd/mkinitrd_command_generator.sh`


### slackware install
* format drive (with cfisk, gparted, parted, ...)
* enter `setup` to start graphical install program
* follow instructions
* yes, hardware clock is set to UTC
* `useradd -m -s /bin/bash foobar`  
* `usermod -aG wheel,audio,video foobar`  
* `passwd foobar`
* `vim /etc/inittab`    
* set default runlevel to 4 to get to greeter
* set wayland as default in greeter
* change greeter kb layout



### slackpkg stuff
* slackpkg download *packageName*     
* location : /var/cache/packages/patches/packages/linux-5.15.63    
* /etc/slackpkg/blacklist    
* kernel-generic-*    
* kernel-huge-*    
* kernel-modules-*    
* kernel-sources    


### install package
* `installpkg packageName.tgz` or `installpk packageName.txz`    


### play mounted dvd : 
* xine dvd:///mnt/dvd/

### sysvinit
* service --status-all     
* service bluetooth stop    

### slackbuild all cores
* export MAKEFLAGS="-j8"

### list installed packages
* `ls -1 /var/log/packages`


### sbopkg
* install from site with installpkg
* sbopkg -r (update database)
* sqg -p gnome-disk-utility
* sbopkg -i gnome-disk-utility
* queues are located @ /var/lib/sbopkg/queues/


### compile ffmpeg with aac enabled
* `ffmpeg -version`
* remove old ffmpeg version `removepkg ffmpeg-4.4.1-x86_64-2.txz`
* create directory ffmpeg, put inside the following files :
* download ffmpeg source (tar.xz) from : https://mirrors.slackware.com/slackware/slackware64-15.0/source/l/ffmpeg/
* download ffmpeg.SlackBuild from same repo

* open ffmpeg.SlackBuild and comment line with :  `aac=""        ; [ "${AAC:-no}" = "no" ]           && aac="--disable-encoder=aac"{}
* add line `aac="--enable-encoder=aac"` just under
* `./ffmpeg.SlackBuild`
* `installpkg ffmpeg-4.4.1-x86_64-2_with_aac.txz`



### install obs
* install ffmpeg with aac enabled, or else can't record
* `sqg -p obs-studio ; sbopkg -i obs-studio`



### update kernel process 
* [link](https://www.linuxquestions.org/questions/blog/tix-592494/using-slackpkg-to-update-slackware-15-uefi-and-making-new-kernel-bootable-38813/)
* in brief :
```
1) `sudo -i`
2) `slackpkg update` 
3) `slackpkg install-new` 
4) `slackpkg upgrade-all` 
5) goto /boot and copy the vmlinux for your new upgraded kernel to /boot/efi/EFI/Slackware (command was `cp /boot/vmlinuz-generic-5.15.117 /boot/efi/EFI/Slackware/vmlinuz`)
6) rebuild your initrd using the command `/usr/share/mkinitrd/mkinitrd_command_generator.sh` which should give you the command to generate initrd that you just copy-paste and run or run `/usr/share/mkinitrd/mkinitrd_command_generator.sh -h`
7) also update /etc/mkinitrd.sh by running the command `/usr/share/mkinitrd/mkinitrd_command_generator.sh > /etc/mkinitrd.conf`
8) copy the newly built initrd to /boot/efi/EFI/Slackware (`cp /boot/initrd.gz /boot/efi/EFI/Slackware/initrd.gz`)
9) edit the file elilo.conf in /boot/efi/EFI/Slackware (not needed ?)
10) change the line "image=vmlinuz" to point to your new vmlinuz (didnt need to do that ?)
11) change the line "initrd=initrd.gz" to point to your new initrd (didnt do either)
12) `eliloconfig`
13) reboot
```

### change mirror
* when `slackpkg update` takes too long
* uncomment 1 url on file /etc/slackpkg/mirrors
* example (for France) : `ftp://nephtys.lip6.fr/pub/linux/distributions/slackware/slackware64-15.0/`


### slackware stable changelog
* [link](http://www.slackware.com/changelog/stable.php?cpu=x86_64)


### qemu complete setup
* `sqg -p libslirp ; sbopkg -i libslirp` (for usermode network)
* install qemu with slirp enabled : (`SLIRP=yes ./qemu.SlackBuild`)
* install libvirt (`sqg -p libvirt ; sbopkg -i libvirt`)
* `/etc/rc.d/rc/libvirt start`
* `usermod -aG users foobar` (kvm group is called "users" group in slackware)
* reboot 
* `qemu-system-x86_64 -enable-kvm -drive file=lubuntu.img -cdrom ~/Downloads/lubuntu-22.04.3-desktop-amd64.iso -smp 1 -m 4G -boot order=dc -boot menu=on`

### hp printer setup
* plug usb cable
* `sudo -i`
* `hp-setup`
* choose "usb printer"
* select device
* ppd file is "drv:///HP/hpcups.drv/hp-deskjet_3630_series.ppd"
* add printer
* print


### slackware 15 package list
* on this link are packages already installed on slackware. These are binary files (.txz), not source files (.tar.xz)
* [packages installed by default](https://mirrors.slackware.com/slackware/slackware64-15.0/slackware64/)


### tar.xz vs txz
* tar.xz contain source code. Need to be compiled before install
* txz are compiled packages, can be installed with `installpkg`









## UBUNTU
* download iso with : `wget -c https://releases.ubuntu.com/22.04.2/ubuntu-22.04.2-desktop-amd64.iso`
* download sha256 with :`wget https://releases.ubuntu.com/22.04.2/SHA256SUMS`  
* "-c" means continue partial download

### install steam
* `sudo dpkg --add-architecture i386`
* `sudo add-apt-repository multiverse`
* `sudo apt update`
* `sudo apt install steam`

### replace nano with vim
* `sudo update-alternatives --config editor`


### add binary to path
binary is : $HOME/path/to/bin
* vim ~/.bashrc
* add line "export PATH="$HOME/path/to:$PATH" 

### change greeter keyboard layout
* `sudo vim /etc/default/keyboard`
* change layout


### french classic kb ?
```
XKBMODEL="pc105"
XKBLAYOUT="fr"
XKBVARIANT=""
XKBOPTIONS=""

BACKSPACE="guess"
```

### install java
* `sudo apt install openjdk-19-jdk`


### ssh
* on server machine : 
* `sudo apt install openssh-server`
* `sudo ufw allow ssh`
* `sudo systemctl start ssh`


* on client machine : 
* `ssh foobar@192.168.1.15`


### ubuntu kiosk (very wip)
* start firefox in kiosk mode : `firefox --kiosk gnu.org`
* application at startup :  `gnome-session-properties`
* settings -> keyboard -> View and Customize Shortcuts -> backspace to unset shortcuts
* sudo apt install gnome-shell-extensions => gnome-shell-extensions -> toggle dock

### change swappiness ubuntu
* = ram usage % at which system will start using swap
* `sudo sysctl vm.swappiness=10`
* `cat /proc/sys/vm/swappiness`













### cron
* cron (working)
```
sudo -i
crontab -e
echo "Hw at $(date) >> /home/foobar/greetings.txt"   # add line at eof
```


* cron (2) (WORKING)
```
sudo -i
crontab -e
@reboot firefox  # add line at eof 
```


### init.d
* init.d (not working) : 
```
chmod +x /path/to/script
sudo mv /path/to/script etc/init.d/
sudo update-rc.d script defaults
```

### better battery
```
sudo systemctl stop power-profiles-daemon.service
sudo systemctl disable power-profiles-daemon.service
sudo systemctl mask power-profiles-daemon.service
```

* `sudo apt install tlp`
* `sudo systemctl enable tlp.service`
* `sudo tlp start`
* `sudo tlp-stat -s`
* `sudo tlp-stat -c`
* `sudo tlp-stat -b` 
* `sudo vim /etc/tlp.conf`

### blacklist kernel module
* `sudo vim /etc/modprobe.d/blacklist.conf`
* add "blacklist <kernel module name>" at end of file













## GPU PASSTHROUGH ATTEMPT

### VGA compatible controller vs Display controller
* 00:01.0 VGA compatible controller: Red Hat, Inc. Virtio 1.0 GPU (rev 01)
* 07:00.0 Display controller: Advanced Micro Devices, Inc. [AMD/ATI] Sun XT [Radeon HD 8670A/8670M/8690M / R5 M330 / M430 / Radeon 520 Mobile] (rev 83)
* gpu passthrough possible with Display controller ?

### https://github.com/kholia/OSX-KVM/blob/master/notes.md#gpu-passthrough-notes 
* `sudo vim /etc/modprobe.d/blacklist.conf`  
* blacklist amdgpu
* blacklist radeon

* `lspci -nnk | grep "AMD"
* => 1002:6660

* `sudo vim /etc/default/grub`
* GRUB_CMDLINE_LINUX_DEFAULT="iommu=pt intel_iommu=on vfio-pci.ids=1002:6660 kvm.ignore_msrs=1 video=vesafb:off,efifb:off"
* `sudo vim /etc/modprobe.d/vfio.conf`
* ...
* sudo update-grub2
* sudo update-initramfs -k all -u
* sudo modprobe vfio-pci
* sudo dmesg | grep -i "iommu"
* sudo dmesg | grep "vfio"
* error message : "vfio-pci 0000:01:00.0: Invalid PCI ROM header signature: expecting 0xaa55, got 0x0000"


### blacklist intel gpu/driver
* vim /etc/default/grub
* default line : `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"`
* vim /etc/modprobe.d/blacklist.conf
* blacklist i915
* => display broken at reboot

### run app using discrete or integrated gpu
* integrated : `glxinfo | grep "OpenGL renderer"`
* discrete : `DRI_PRIME=1 glxinfo | grep "OpenGL renderer"` 

### gpu passthrough <=> IOMMU
* guest edition windows : https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/virtio-win-0.1.266-1/
* https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF

















## OTHER
### hard drive inches
* 2.5 inches : small laptop harddrives
* 3.5 inches : bigger desktop harddrives

### unplug internal hard drive
* never unplug a sata hard drive while system is up. Even if is not the main drive because data is cached    

### wifi chip, bluetooth chip
* on laptops, wifi chip is also bluetooth chip
* wifi antennas are used for wifi and bluetooth
* 2 antenna terminals are called MAIN and AUX. If only 1, plug into main
* wires are interchangeable (probably)
















## LFS

### starting distro ?
* slackware with only A packs ?




















## QEMU/VIRTUALIZATION


### qemu seems to need libvirt on slackware

### qemu stuff
* `qemu-img create debian_server.img 20G`
* `qemu-system-x86_64 -hda debian_server.img -cdrom ~/Downloads/debian-11.6.0-amd64-netinst.iso -smp 1 -m 2G -boot order=dc`

* `qemu-img create lubuntu.img 50G`
* `qemu-system-x86_64 -drive file=lubuntu.img -cdrom ~/Downloads/lubuntu-22.04.3-desktop-amd64.iso -smp 1 -m 4G -boot order=dc -boot menu=on` (too slow. Need kvm)
* `qemu-system-x86_64 -enable-kvm -drive file=lubuntu.img -cdrom ~/Downloads/lubuntu-22.04.3-desktop-amd64.iso -smp 1 -m 4G -boot order=dc -boot menu=on` (<-- this one)
* `qemu-system-x86_64 -enable-kvm -drive file=lubuntu.img -cdrom ~/Downloads/lubuntu-22.04.3-desktop-amd64.iso -smp 1 -m 4G -boot order=dc -boot menu=on -nic users,ipv6=off,model=e1000,mac=52:54:98:76:54:32` ("Parameter 'type' does not accept value 'users'")
* `qemu-system-x86_64 -enable-kvm -drive file=lubuntu.img -cdrom ~/Downloads/lubuntu-22.04.3-desktop-amd64.iso -smp 1 -m 4G -boot order=dc -boot menu=on -nic none` (no internet connection)
* FIXME : specify raw for file lubuntu.img

### "failed to initialize kvm: Permission denied"
* `sudo usermod -aG kvm foobar`
* `sudo usermod -aG libvirtd foobar`
* `sudo usermod -aG users foobar`


### install virt-manager
* sudo apt install virt-manager
* sudo systemctl start libvirtd
* reboot
* virt-manager


### check qemu commands sent by virtmanager
* vim /var/log/libvirt/qemu/$DOMAIN_NAME.log
* qemu-img create -f qcow2 Bliss.qcow2 20G


### android - blissos - install
* Bliss-v14.10.3-x86_64-OFFICIAL-opengapps-20240916.iso
* os type : android-x86-9.0
* partition type : msdos
* 1 partition : linux ; make bootable ; ext4 ; 20Gb



















## SAMBA

### Bad. Dont do
* `sudo -i`
* `apt install samba`
* `systemctl status smbd`
* `mkdir /home/shared`
* `chmod 777 /home/shared/`
* `ufw allow samba`
* `cp /etc/samba/smb.conf /etc/samba/smb.conf.backup`
* `vim /etc/samba/smb.conf`
```
[shareddir]
   comment = a shared dir
   path = /home/shared
   public = yes
   only guest = yes
   writable = yes
   printable = no   
```
* `testparm` to test samba conf
* access shared folder `smb://<deviceip>/shareddir` 




