# PORTEUS JOURNAL STUFF

## links
* [search](https://search.brave.com/search?q=porteus+broadcom)
* [porteus home page](http://www.porteus.org/)
* [forum](https://forum.porteus.org/)
* [download page] (http://ftp.vim.org/ftp/os/Linux/distr/porteus/x86_64/)
* [cheat codes](http://www.porteus.org/tutorials/26-general-info-tutorials/117-cheatcodes-what-they-are-and-how-to-use-them.html)




## broadcom driver issues
### laptop specs
[lenovo laptop specs](https://support.lenovo.com/us/en/solutions/pd025242-overview-lenovo-b570e)
### wireless card specs
`Broadcom Corporation BCM4313 802.11bgn Wireless Network Adapter [14e4:4727] (rev 01)`
### links
* https://forum.porteus.org/viewtopic.php?t=9971
* https://forum.porteus.org/viewtopic.php?t=9118&start=30
* https://forum.porteus.org/viewtopic.php?t=9181
* https://forum.porteus.org/viewtopic.php?t=8330
* https://forum.porteus.org/viewtopic.php?t=10000
### solution
`problem is in this file : `        
/etc/modprobe.d/broadcom_blacklist.conf     

in file, remove all modules from blacklist    
et voilà !


## cheatcodes/bootoptions
file loaded at startup : /mnt/sdb1/boot/syslinux/porteus.cfg     
append cheatcodes in here ?




## install tricks

## persistence 

## path stuff

## kernel & modules stuff
kernel modules end in .ko

`uname -r`  : 5.18.8-porteus    
`lspci -knn`     

what is `06-crippled_sources-5.18.8-64bit.xzm` used for ?

modules in `porteus/modules` directory will be loaded at startup


## keyboard
`loadkeys fr`


## packages & stuff
### convert package
`txz2xzm packageName.tgz`

### activate module 
`activate packageName.xzm`
