# PIXDRAW DEVLOG

## 8/5/23
* [triangle rasterization algorithm](http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html#algo2)
* [other link](https://gabrielgambetta.com/computer-graphics-from-scratch/07-filled-triangles.html#drawing-filled-triangles) 
* [link 3](https://www.geeksforgeeks.org/check-whether-a-given-point-lies-inside-a-triangle-or-not/)

* working on fill rectangle fn
* use bresenham to draw horizontal lines ?

1. get points
2. order points
3. draw points inbetween


### 8/6/23
* tried to implement fill triangle using algo from first link. Not working
```
fn drawTriangle(canvas: Canvas, x1: i32, y1: i32, x2: i32, y2: i32, x3: i32, y3: i32) void {
    const dx2 = (x2 - x1);
    const dy2 = (y2 - y1);

    const dx3 = (x3 - x1);
    const dy3 = (y3 - y1);

    print("{d}x{d} {d}x{d} {d}x{d}\n", .{ x1, y1, x2, y2, x3, y3 });
    print("{d} {d} {d} {d}\n", .{ dx2, dy2, dx3, dy3 });

    const invslope1 = @divFloor(dx2, dy2);
    const invslope2 = @divFloor(dx3, dy3);

    var curx1 = x1;
    var curx2 = x1;

    var scanlineY = y1;
    while (scanlineY <= y2) : (scanlineY += 1) {
        drawLineBresenham(canvas, curx1, scanlineY, curx2, scanlineY, 1, Colors.black.toHex());
        curx1 += invslope1;
        curx2 += invslope2;
    }
}
```


```
                            drawTriangle(layers.items[curr_layer_nb - 1], 5, 5, 5, 35, 50, 35); //ok
                            // drawTriangle(layers.items[curr_layer_nb - 1], 60, 4, 5, 35, 80, 35);

                            layers.items[curr_layer_nb - 1].pixels[5 + 5 * 200] = Colors.red.toHex();
                            layers.items[curr_layer_nb - 1].pixels[5 + 35 * 200] = Colors.red.toHex();
                            layers.items[curr_layer_nb - 1].pixels[50 + 35 * 200] = Colors.red.toHex();

```


