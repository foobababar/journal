# ZIGLOG

## for loop
```
const items = [_][]const u8{ "foo", "bar", "baz" };
for (items) |item| {
    print("{s}\n", .{item});
}

for (items, 0..) |_, i| {
    print("{d}\n", .{i});
}

for (items, 0..) |item, i| {
    print("{d} -> {s}\n", .{ i, item });
}
```

## while loop


## array to slice
```
const arr = [_]u8{ 'f', 'o', 'o', 'b', 'a', 'r' };
const slice1: []const u8 = arr[0..];
var slice2: []const u8 = arr[0..];
```



## int to string
```
const int = 420;
var buffer = std.mem.zeroes([10]u8); // we need a buffer to format the string
const string = try std.fmt.bufPrint(buffer[0..], "{d}", .{int});
print("{s}\n", .{string});
```


## string to int
```
const string2 = "720";
const int2 = try std.fmt.parseInt(u32, string2, 10);
print("{d}\n", .{int2});
```





## array of functions

```
fn foo() void {
    print("foo\n", .{});
}

fn bar() void {
    print("bar\n", .{});
}

fn baz() void {
    print("baz\n", .{});
}
```


```
const array_fns = [_]fn () void{ foo, bar, baz };
array_fns[0]();
```



