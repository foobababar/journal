# java journal

## basic concepts

### foreach
iterate over array or ArrayList

```
int[] array = new int[]{0, 1, 2, 3, 4}; 
for(int i : array){
  System.out.println(i);
}
```

### interfaces
Contract. Programmer has to provide every method on the interface. \
Keyword "implements". \
Abstract class. \
Methods with empty bodies. \
1 class can implement multiple interfaces. \


```
public interface Coffee {
  public boolean isArabica();
  public boolean isRobusta();
  public void characteristics();
}
```

```
public class Arabica implements Coffee {
  //can have non-empty constructor !
  @Override
  public boolean isArabica(){
    return true;
  }
  //
  @Override
  public boolean isRobusta(){
    return false;
  }
  //
  @Override
  public void characteristics(){
    System.out.println("Sweet & enjoyable");
  }
}
```

```
public class Main {
  public static void main(String[] args){
    Arabica myCoffee = new Arabica();
    System.out.println(myCoffee.isArabica());
    myCoffee.characteristics();
  }
}
```



## advanced concepts

### generics
```
public static <T> void p(T log){
  System.out.println(log);
}
```

### static class

use methods of object without intantiating new object

```
public class PrinterObj {
  public static <T> void print(T obj){
    System.out.println(obj); 
  }
}
```

```
public class Main {
  public static void main(String[] args){
    PrinterObj.print("Hello world !"); 
  }
}
```

### hashtable

```
import java.util.Hashtable;
//...
//
Hashtable<String, String> table = new Hashtable<>();
table.put("google", "passwd123");
table.put("amazon", "cancer123");
table.put("facebook", "spyware123");
//
System.out.println(table.size()); //3
//
String tmp;
if((tmp = table.get("amazon")) != null) System.out.println(tmp);
//
if(table.containsKey("facebook")) System.out.println(table.get("facebook"));
if(table.containsValue("passwd123")) System.out.println("table contains google password");
//
table.remove("google");
```


### lambda
TODO

## conversions
### char -> int

System.out.println(Character.getNumericValue('2')+7); // = 9

## bits, bytes & co
### bits
```
byte b1 = 0b01;
byte b2 = 0bff;
```

### declare byte
```
byte b1 = 127; //max byte value
byte b2 = -126; //min byte value
byte b3 = (byte) 255;
```

### print byte
```
byte b = (byte) 255;
System.out.printf("0x%02X", b); //0xFF
```

### byte overflow
```
byte b1 = (byte) 255;
byte b2 = (byte) 2;
byte b3 = (byte) (b1+b2); //0x01
//
byte b4 = (byte) 4;
byte b5 = (byte) (b2-b4); //0xfe
```

### bitwise operators

```
byte b1 = 0b11; //3 <=> 00000011
byte b2 = 0b01; //1 <=> 00000001
byte b3 = b1 & b2; // 00000001
//
byte b4 = b1 | b2; //00000011
//
byte b5 = b1 << 1; //00000110
//
byte b6 = 0b10;
byte b7 = b6 >> 1; //0b01
byte b8 = b6 >> 2; //0b00 
```

## files & co
### read file into byte[]

```
File path = new File("bytes");
byte[] arr = new byte[(int) path.length()];
//
try (FileInputStream stream = new FileInputStream(path)){ 
  stream.read(arr);
}
catch(IOException e){
  e.printStackTrace();
}
```


## Other

### design patterns ; recurring patterns

* Solutions to common coding problems.
* Algorithms to solve common coding problems

#### Visitor pattern
TODO


### export jar file

classes are : Main.class Processor.class \
`jar cf Ctp.jar *.class` \
`java -cp Ctp.jar Main` \


### command line arguments

```
public static void main(String[] args){
  System.out.println(args.length);
  System.out.println(args[0]);  //first argument
}
```