# MATH

## graphs & stuff

### slope of line
dy/dx

### naive line drawing algorithm/equation 
[wikipedia on line drawing](https://en.wikipedia.org/wiki/Line_drawing_algorithm)

### proof
equation of line between (x1, y1) and (x2, y2)

y = ax+b    (1) 
dy = y2-y1  (2) 
dx = x2-x1  (3) 
a = dy/dx   (4) 

(1) & (4) : y = dy/dx * x + b (5)

at point (x1, y1) : y1 = dy/dx * x1 + b
		    b = y1 - dy/dx * x1  (6)

(5) and (6) : y = dy/dx * x + y1 - dy/dx * x1
              y = dy/dx * (x - x1) + y1
              y = dy * (x - x1) /dx + y1 




### Functions

### Fibonacci
* "In mathematics, the Fibonacci sequence is a sequence in which each number is the sum of the two preceding ones."
* 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...

### Log 
log vs ln ...

### Ln
* defined on ]0; +inf[
* forall x € R, ln(e^x) = x
* forall x > 0, e^(lnx) = x
* ln 1 = 0
* ln e = 1

see graph...


### Exp
* forall x € R, exp(x) = e^x > 0
* f(0) = 1
* forall x € R, ln(e^x) = x
* forall x > 0, e^(lnx) = x

see graph...

## possible combinations
* 2^2 : 00 01 10 11 = 2^2 = 4 possible combinations
* 2^3 : 000 001 010 011 100 101 110 111 = 2^3 = 8 possible combinations

so formula is : base^number_of_digits

so for example, for RGB colors (0 -> 255) : nb of color combinations = 256^3 = 16 777 216

## matrix

```
    [1 1] 
A = [1 1]    B = [1 1 1]
    [1 1]        [1 1 1]

```
`rows x columns` \
A is 3x2 = three by two \
B is 2x3 = two by three




### matrix +

```

C = [0 1]
    [2 3]

D = [4 5]
    [6 7]


C+D = [(0+4) (1+5)] = [4  6]
      [(2+6) (3+7)]   [8 10]

```


### matrix *

```
     [3 3]
3A = [3 3]
     [3 3]
```


```
C*D = [(0*4+1*6) (0*5+1*7)] = [ 6  7]
      [(2*4+3*6) (2*5+3*7)]   [26 31]

```
**number of columns in first matrix must be equal to nb or rows in second matrix** \
**(x * a).(a * y) OK**




## references
* https://en.wikipedia.org/wiki/Fibonacci* https://www.khanacademy.org/math/algebra-home/alg-matrices
