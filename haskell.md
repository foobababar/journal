# LEARNING HASKELL

## references 
* http://learnyouahaskell.com/chapters
* http://learnyouahaskell.com/
* https://www.haskell.org/documentation/
* https://www.youtube.com/watch?v=02_H3LjqMr8&t=170s

## how to
* haskell interpretor : ghci

## very simple code
``` 
True && False -- false
True || False -- true
```


```
succ 23 -- ==24
min 14 15
max 12 112
```


```
succ 13 * 2 -- 28
succ (13*2) -- 27

```    

```
"hello" == "hello" -- True
succ 12 - (max 2 3) *2 -- 7
```

## functions
```
addMe x y = x+y -- define function
addMe 4 5 -- 9

doubleMe x = x*2
doubleUs x y = doubleMe x + doubleMe y
doubleUs 2 3 -- 10

```

## if statement
```
isBig x = if x>500 then True else False
isBig 499 -- False
isBig 501 -- True

```

## lists
```
let numbers = [0,1,2,3]
let numbers2 = [4,5,6]
numbers ++ numbers2 -- 

```
* string are lists of characters.

```
let hw = ['h', 'e', 'l', 'l', 'o']
hw ++ "world" -- "helloworld"

'#':(hw ++ "world") -- add at start of list

hw !! 0 -- get item at index 0

```

## more operations on lists
```
let nb = [0,1,2,3]
head nb -- O

tail nb -- [1,2,3]

last nb -- 3

init nb -- [0,1,2]
```

## even more operations on lists
```
let lst = [0,1,2,3]
length lst -- 4


let lst2 = []
null lst -- False
null lst2 -- True


reverse lst -- [3,2,1,0]

take 2 lst -- extract 2 elements from start of lst

drop 2 lst -- remove 2 elements from start of lst

minimum lst
maximum lst

sum lst
product lst


3 `elem` lst -- True
4 `elem` lst -- False
```

## ranges 
```
[0..14]

['a'..'z']

['B'..'O']

[0,3..18] -- range from 0 to 18 with step 3


cycle [0,1,2] -- infinite
cycle [0..2] 

take 10 (cycle [0,1,2])

repeat 3 -- infinite list of 3

take 2 (repeat 3) -- [2,2]

replicate 14 30 -- creates list of 14 40s

```

## list comprehension

```
[x*2 | x <- [1..5]] -- [2,4,6,8,10]

[x*2 | x <- [1..5], x /= 1] -- /= means !=

[x*2 | x <- [1..5], x*2 > 4]

[x | x <- [0..50], x `mod` 2 == 0]
```

## list comprehension 2
```
foobar lst = [if x < 10 then "foo" else "bar" | x <- lst, odd x]
foobar [0..15]


[x | x <- [0..25], x>10, x<18]

[x*y | x <- [0..3], y <- [4,5], x*y /= 0]

let nouns = ["peasant", "paradise", "smartass"]
let adjs = ["rich", "enlightened", "boring", "bored"]
[adj ++ " " ++ noun | adj <- adjs, noun <- nouns]

length2 x = sum [1 | _ <- x]
length2 "foobar"

removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']] 
removeNonUppercase "FFFoooBaaaRRR"

let xxs = [[1,3,5,2,3,1,2,4,5],[1,2,3,4,5,6,7,8,9],[1,2,4,2,1,6,3,1,3,2,3,6]]
[ [ x | x <- xs, even x ] | xs <- xxs] -- what is this sorcery ?

```

## Tuples

```
let triple = ("zero", 0, "0")

let tuple = ("zero", 0)
fst tuple -- "zero"

snd tuple -- 0

zip [0..2] [3..5] -- [(0,3),(1,4),(2,5)]

zip [0..2] [3..7] -- [(0,3),(1,4),(2,5)]

zip [0..] ["zero", "one", "two", "three", "four"] -- [(0,"zero"),(1,"one"),(2,"two"),(3,"three"),(4,"four")]

```

## Tuples 2
* Problem : which right triangle that has integers for all sides and all sides equal to or smaller than 10 has a perimeter of 24?

```
let triangles = [ (a,b,c) | c <- [1..10], b <- [1..10], a <- [1..10] ]
let rightTriangles = [ (a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2 + b^2 == c^2]
let rightTriangles' = [ (a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2 + b^2 == c^2, a+b+c == 24]

```

## Typing
* type inference exists
* types known @ comptime

```
:t True -- True :: Bool
:t "txt -- "txt" :: [Char]
:t 72 -- 72 :: Num p => p
```


## function signature
* note : on ghci, use multiline command blocks
```
:{
double :: Int -> Int
double x = 2*x
:}

double 75 -- 150

```

## Int vs Integer

## Float vs Double
:{
circum :: Float -> Float
circum r = 2*pi*r
:}

circum 12 -- 75.398224


:{
circum2 :: Double -> Double
circum2 r = 2*pi*r
:}

circum2 12 -- 75.39822368615503

## type variables
get information on function
```
:t head -- head :: [a] -> a
:t fst -- fst :: (a,b) -> a

```
## typeclasses


## in practice
file script.hs :

```
module Main where

doubleMe :: Int -> Int
doubleMe x = 2*x


main = 
    do

    let lst = [1,0,3,75,14,76,23,89]
    print(doubleMe 72)

```
ghc script.hs

Ord means "something than can be ordonned"

## quicksort 
from http://learnyouahaskell.com/recursion#quick-sort

```
quicksort :: (Ord a) => [a] -> [a]  
quicksort [] = []  
quicksort (x:xs) =   
    let smallerSorted = quicksort [a | a <- xs, a <= x]  
        biggerSorted = quicksort [a | a <- xs, a > x]  
        in  smallerSorted ++ [x] ++ biggerSorted  

```

## stuff
```
foo = 9 :: Int
sqrt (fromIntegral foo) -- 3.0
```

```
 not(True)
```


```
numbers = 1:4:7:[]   -- [1,4,7]
```








