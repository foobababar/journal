# NCURSES STUFF

## GOALS
* statically link ncurses
* link program with different ncurses
* make ncurses into a single, self contained, C file

## links
* [ncurses home page](https://ftp.gnu.org/gnu/ncurses/)
* [cool build instructions](https://www.reddit.com/r/C_Programming/comments/fbc2kp/i_cant_compile_an_ncurses_program_statically/)



## gcc flags
* -L : 	add path to gcc search location. ex: gcc -L /path/to/lib (will search for .so files ??????)
* -I : add directory path for header files (.h). ex: gcc -I /path/to/include main.c -o out
* -l :

`-L before -l`

## file extensions
* .so : shared object
* .a : static library
* .o : object file


## tips
* find all references to "initscr" in present directory : `grep -r "initscr" .`


## HW

`gcc hw.c -lncurses -o out`

```
#include <ncurses.h>

int main()
{	
	initscr();			/* Start curses mode 		  */
	printw("Hello World !!!");	/* Print Hello World		  */
	refresh();			/* Print it on to the real screen */
	getch();			/* Wait for user input */
	endwin();			/* End curses mode		  */

	return 0;
}

```

`ldd ./out` :

```
        linux-vdso.so.1 (0x00007ffd341ae000)
        libncurses.so.6 => /lib64/libncurses.so.6 (0x00007f1086410000)
        libtinfo.so.6 => /lib64/libtinfo.so.6 (0x00007f10863df000)
        libc.so.6 => /lib64/libc.so.6 (0x00007f1086200000)
        libdl.so.2 => /lib64/libdl.so.2 (0x00007f10861fb000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f1086474000)

```



## build process
* download ncurses-6.4
* untar xvf ncurses-6.4
* cd ncurses-6.4
* ./configure
* make

## gcc build process
1. preprocessing
2. compiling 
3. assembling
4. linking







