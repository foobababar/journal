export PATH=$PATH:~/apps/zig-linux-x86_64-0.11.0-dev.2336+5b82b4004
# ^ add zig binary to path like so 

alias hw="echo hello world !"
alias blender="/opt/blender/blender"

alias vi="nvi"
alias javac="/usr/lib64/java/bin/javac"
alias java="/usr/lib64/java/bin/java"


alias mount1="sudo mount /dev/sdb1 /mnt/tmp"
alias umount1="sudo umount /mnt/tmp"
alias cd1="cd /mnt/tmp"

alias simple="xrandr --output HDMI-1 --off"
alias double="xrandr --output HDMI-1 --auto && xrandr --output HDMI-1 --left-of eDP-1 && xrandr --output eDP-1 --primary"
alias uno="xrandr --output eDP-1 --auto"

export HISTSIZE="1000000000000" 
export HISTFILESIZE="1000000000000" 

alias wac0="xsetwacom --list devices"

function ctp () {
  [[ $# -ne 2 ]] && echo "expected exaclty 2 arguments" && return 1
  java -cp ~/code/java/Ctp.jar Main "$1" "$2"
}

function fwac() {
  [[ $# -ne 2 ]] && echo "expected exactly 2 arguments" && return 1
  xsetwacom --set "$1" mapToOutput eDP-1 && xsetwacom --set "$2" mapToOutput eDP-1
}

